package model;

public class Rechteck {
//Anfang der Atribute
	 private int x;
     private int y;
     private int breite;
     private int hoehe;
     
     public int getX() {
       return x;
     }

     public int getY() {
       return y;
     }

     public int getBreite() {
       return breite;
     }

     public int getHoehe() {
       return hoehe;
     }

     public Rechteck() {
       super();
       this.x=0;
       this.y=0;
       this.breite=0;
       this.hoehe=0;
       
     }

     public Rechteck(int x, int y, int breite, int hoehe) {
       super();
       this.x=x;
       this.y=y;
       this.breite=breite;
       this.hoehe=hoehe;
       
     }

     public void setX(int xNeu) {
       x = xNeu;
     }

     public void setY(int yNeu) {
       y = yNeu;
     }

     public void setBreite(int breiteNeu) {
       breite = breiteNeu;
     }

     public void setHoehe(int hoeheNeu) {
       hoehe = hoeheNeu;
     }
}

